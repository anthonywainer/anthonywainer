from registration.backends.hmac.views import RegistrationView
from django.http import HttpResponseBadRequest
from django.contrib.auth import authenticate, login, logout
from django.contrib import auth
from django.http import HttpResponseRedirect
from django.views.generic import FormView
from django.core.urlresolvers import reverse_lazy

from apps.security.forms import LoginForm, UsersRegisterForm
import json

class login(FormView):
    form_class  = LoginForm
    template_name = "login.html"
    success_url = reverse_lazy('index')
    def get_context_data(self, **kwargs):
        context = super(login, self).get_context_data(**kwargs)
        #context['bene']=b
        return context

    def dispatch(self, request, *args, **kwargs):

        if request.user.is_authenticated():
            return HttpResponseRedirect(self.get_success_url())
        else:
            return super(login, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        username = self.request.POST['username']
        password = self.request.POST['password']
        user = authenticate(username=username, password=password)
        auth.login(self.request, user)
        return super(login, self).form_valid(form)

    def form_invalid(self, form):
        ee = {}
        if form.errors:
            for i in form.errors:
                e = form.errors[i]
                ee[i] = str(e)
        return HttpResponseBadRequest(json.dumps(ee))

def LogOut(request):
    logout(request)
    return HttpResponseRedirect('/')

class register(RegistrationView):
    form_class  = UsersRegisterForm
    template_name = "form_regis_user.html"

    def get_success_url(self, user):
        return ('/security/usuarios/completado', (), {})

    def form_invalid(self, form):
        ee = {}
        if form.errors:
            for i in form.errors:
                e = form.errors[i]
                ee[i] = str(e)
        return HttpResponseBadRequest(json.dumps(ee))

    def create_inactive_user(self, form):

        new_user                   = form.save(commit=False)
        new_user.is_active         = True
        new_user.username          = form.cleaned_data["username"]
        new_user.email             = form.cleaned_data["email"]
        new_user.password_default  = form.cleaned_data["password2"]
        new_user.set_password(form.cleaned_data["password2"])
        new_user.save()

        for i in self.request.POST.getlist('profiles'):
            new_user.profiles.add(int(i))

        return new_user