from django.contrib.auth.decorators import login_required
from django.shortcuts import render,get_object_or_404, HttpResponse
from rest_framework import permissions
from rest_framework.decorators import api_view, renderer_classes, permission_classes
from rest_framework.renderers import JSONRenderer,TemplateHTMLRenderer
from rest_framework.response import Response

from apps.core.error import error
from apps.security.forms import profileAdd
from apps.security.models import profile, detail_permission_profile_modules
from apps.security.serializers import profileSerializer


@login_required(login_url='login/')
@api_view(['GET','POST'])
@permission_classes((permissions.AllowAny,))
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def profiles(r):
    p = profile.objects.filter(status = True)
    f = profileAdd()
    if r.method == 'POST':
        if r.POST.get("event")!="":
            idp = int(r.POST.get("event"))
            
            a = get_object_or_404(profile,pk=idp)
            f = profileAdd(r.POST, instance=a)

            dp = detail_permission_profile_modules.objects.filter(idprofile=idp)
            idper = [a.idmodules_id for a in dp]
            
            if f.is_valid():
                mod = r.POST.getlist('module')
                p = profile.objects.get(pk=idp)
                p.description = r.POST['description']
                p.save()
                for i in mod:
                    if (int(i) in idper) == 0 :
                        m1 = detail_permission_profile_modules(idprofile_id=idp, idmodules_id=int(i))
                        m1.save()
                    #else:
                for a in dp:
                    if (str(a.idmodules_id) in mod) == 0:
                        get_object_or_404(detail_permission_profile_modules,pk=a.id).delete()
            else:
                return error(f)

             
        else:
            f = profileAdd(r.POST or None) #recogiendo los datos post para ser validado en el formulario
            if f.is_valid():
                pe = profile.objects.create(description = r.POST['description'])
                for i in r.POST.getlist('module'):
                    m1 = detail_permission_profile_modules(idprofile=pe, idmodules_id=int(i))
                    m1.save()
            else:
                return error(f)

    elif r.accepted_renderer.format == 'json':
        serializers = profileSerializer(instance = p, many = True )
        data = serializers.data
        return Response(data)

    return render(r, 't_profile.html')

@login_required(login_url='login/')
def editprofile(r):
    id = r.GET.get("id")
    if id != "":
        a=get_object_or_404(profile,pk=int(id))
        f=profileAdd(instance=a)
        t= 'ACTUALIZAR'
        b= 'Actualizar'
    else:
        f=profileAdd()
        t= 'AGREGAR NUEVO'
        b= 'Registrar'
    return render(r, 'frm_profile.html', {'frm':f, 'title':t, 'btn':b,'idprofile':id})

@login_required(login_url='login/')
def deleteprofile(em, id):

    dp = detail_permission_profile_modules.objects.filter(idprofile=id)
    for a in dp:
        get_object_or_404(detail_permission_profile_modules,pk=a.id).delete()

    get_object_or_404(profile,pk=id).delete()

    return HttpResponse('DATOS ELIMINADOS')