from django.shortcuts import render, HttpResponse
from apps.security.models import detail_permission_profile_modules

def permissionView(e):
    if e.GET.get('id') is not None:
        idprofile = e.GET.get('id')
        name = e.GET.get('name')
        permiso  = detail_permission_profile_modules.objects.select_related('idmodules').filter(idprofile=idprofile, idmodules__status=True
            ).values('id',
            'See','Search','Add','Update','Delete','Print','Export','Import',
            'idmodules_id','idmodules__description','idmodules__father','idmodules__icon__clase')
        return render(e, 't_permission_module.html', {'permission':permiso,'name':name,'idp':idprofile})


def CambiarEstado(r):
    ide = r.GET.get('id')
    nombre = r.GET.get('name')
    es = r.GET.get('estado')
    idper = r.GET.get('id_per')
    if es=='false':
        estado=False
    else:
        estado=True
        
    e = detail_permission_profile_modules.objects.get(pk= int(ide), idprofile_id=int(idper)) #el permite guardar

    if nombre == 'See':
        e.See = estado
    elif nombre == "Search":
        e.Search = estado
    elif nombre == "Add":
        e.Add = estado
    elif nombre == "Update":
        e.Update = estado
    elif nombre == "Print":
        e.Print = estado
    elif nombre == "Export":
        e.Export = estado
    elif nombre == "Import":
        e.Import = estado
    elif nombre == "Delete":
        e.Delete = estado
    e.save()
    return HttpResponse('Recibido')

def CambiarEstadoTotal(m):
    des   = m.GET.get('des')
    idper = m.GET.get('id_per')
    estado = m.GET.get('estado')
    if estado=='False':
        estado=False
    print(idper)
    p = detail_permission_profile_modules.objects.select_related('idmodules').filter(idprofile=int(idper),idmodules__status=True,idmodules__father=int(des))

    for x in p:
        a = detail_permission_profile_modules.objects.get(pk=int(x.id), idprofile_id=int(x.idprofile_id))
        a.See        = estado
        a.Search     = estado
        a.Add    = estado
        a.Update = estado
        a.Print   = estado
        a.Export   = estado
        a.Import   = estado
        a.Delete   = estado
        a.save()
   
    return HttpResponse('Recibido')
