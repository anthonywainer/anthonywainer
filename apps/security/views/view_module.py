from braces.views import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import render, get_object_or_404, HttpResponse
from django.views.generic import TemplateView
from django.http import HttpResponseBadRequest
from rest_framework import permissions
from rest_framework.decorators import api_view, renderer_classes, permission_classes
from rest_framework.renderers import JSONRenderer,TemplateHTMLRenderer
from rest_framework.response import Response

from apps.security.forms import UserChangeForm, formFoto, moduleAdd, submoduleAdd
from apps.security.models import icons, Users, modules
from apps.security.serializers import iconosSerializer, modulesSeria
import json
def error(f):
    ee = {}
    for i in f.errors:
        e = f.errors[i]
        ee[i] = str(e)
    return HttpResponseBadRequest(json.dumps(ee))

@login_required(login_url='login/')
@api_view(['GET','POST'])
@permission_classes((permissions.AllowAny,))
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def module(m):
    o = modules.objects.select_related('icon').filter(status = True).values(
        'id','description','url','father',
        'icon__clase'
        )
    f  = moduleAdd()
    if m.method == 'POST':
        f = moduleAdd(m.POST)
        if f.is_valid():
            f.save()
        else:
            return error(f)

    elif m.accepted_renderer.format == 'json':
        o = modules.objects.select_related('icon').filter(status = True)
        serializers = modulesSeria(instance = o, many = True )
        data = serializers.data
        return Response(data)

    return render(m, 't_module.html', {'module': o, 'formulario': f})

@login_required(login_url='login/')
def deletemodule(em, id):
    e = modules.objects.get(pk = id)
    e.status = False
    e.save()
    return HttpResponse(e.father)


@login_required(login_url='login/')
@api_view(['GET','POST'])
@permission_classes((permissions.AllowAny,))
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def updatemodule(ed, id):

    if ed.accepted_renderer.format == 'json':
        su = modules.objects.select_related('icon').filter(status = True,id = id)
        serializers = modulesSeria(instance = su, many = True )
        data = serializers.data
        return Response(data)
    if ed.method == 'GET':
        a=get_object_or_404(modules,pk=id)
        f = submoduleAdd(ed.GET, instance=a)
        if f.is_valid():
            f.save()
        else:
            return error(f)
    return HttpResponse('DATOS EDITADOS')


@login_required(login_url='login/')
@api_view(['GET','POST'])
@permission_classes((permissions.AllowAny,))
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def editamh(r):
    idm=r.GET.get("idmh")
    dato=modules.objects.select_related('icon').filter(id=idm)
    serializers = modulesSeria(instance = dato, many = True )
    hdata = serializers.data
    return Response(hdata)


@login_required(login_url='login/')
@api_view(['GET','POST'])
@permission_classes((permissions.AllowAny,))
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def submodule(sub,id):
    if sub.method == 'POST':
        f = submoduleAdd(sub.POST)
        if f.is_valid():
            f.save()
        else:
            return error(f)

    if sub.accepted_renderer.format == 'json':
        su = modules.objects.select_related('icon').filter(status = True, father = id)
        serializers = modulesSeria(instance = su, many = True )
        data = serializers.data
        return Response(data)

    return HttpResponse('YA ENCONTRE SUBmodule')

@login_required(login_url='login/')
@api_view(['GET','POST'])
@permission_classes((permissions.AllowAny,))
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def fonts(r):
    #m = main()
    t  = r.GET.get("q")
    pg = r.GET.get("page")

    if t is not None:
        p = icons.objects.filter( Q(description__contains=t) )[:pg]
    else:
        p = icons.objects.all()[:pg]
    #p=icono.objects.filter(descripcion__constains=t)

    serializers = iconosSerializer(instance = p, many = True )
    data = serializers.data
    return Response(data)

