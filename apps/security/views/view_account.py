import json
import random
import string

from braces.views import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseBadRequest
from django.shortcuts import render, HttpResponse,get_object_or_404
from django.views.generic import TemplateView, UpdateView
from registration.backends.hmac.views import RegistrationView
from rest_framework.decorators import api_view, renderer_classes, permission_classes
from rest_framework.renderers import JSONRenderer,TemplateHTMLRenderer

#esto es par
from django.contrib.auth import get_user_model
User = get_user_model()

from rest_framework import permissions
from apps.security.models import Users
from apps.security.forms import cueform, formFoto,UsuarioCreationForm
from apps.core.pagination.num_pag import paginacion

@login_required(login_url='login/')
def cuentita(m):
    return render(m, 'cuenta.html')

@login_required(login_url='login/')
@api_view(['GET','POST'])
@permission_classes((permissions.AllowAny,))
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def cuentaedi(q):
    a = q.user.id
    fa=get_object_or_404(Users,pk=int(a))
    f=cueform(instance=fa)
    return render(q, "formcuenta.html", {"f":f})

def datoscuenta(q):
    return render(q, "datoscuenta.html")
#contraseña aleatoria
def id_generator(size=8, chars=string.ascii_uppercase + string.digits + string.punctuation ):
    return ''.join(random.choice(chars) for _ in range(size))

@login_required(login_url='login/')
def list_users(r):
    template_name = "t_users.html"
    u = Users.objects.filter(status=True)
    l= {'search': "'Users','[\"names\",\"last_name\",\"dni\",\"first_name\"]'"}
    v = {'values': '\'"names","last_name","first_name","dni","username","profiles"\''}
    l.update(v)
    return paginacion(r,u,"",template_name,l)


class about_users(LoginRequiredMixin, TemplateView):
    login_url= reverse_lazy('error404')
    template_name = "about_user.html"
    def get_context_data(self, **kwargs):
        context = super(about_users, self).get_context_data(**kwargs)
        u = Users.objects.filter(status=True, id= self.request.GET.get("id"))[:1]
        context['users'] = u

        return context


@login_required(login_url='login/')
def deleteuser(em, id):
    e = Users.objects.get(pk = id)
    e.status = False
    e.save()
    return HttpResponse("eliminado correctamente")

class register_users(LoginRequiredMixin, RegistrationView):
    login_url= reverse_lazy('error404')
    form_class  = UsuarioCreationForm
    template_name = "form_user.html"

    def get_context_data(self, **kwargs):
        context = super(register_users, self).get_context_data(**kwargs)
        context['name'] = 'Registrar'

        return context
    def get_success_url(self, user):
        return ('/security/usuarios/completado', (), {})

    def form_invalid(self, form):
        ee = {}
        if form.errors:
            for i in form.errors:
                e = form.errors[i]
                ee[i] = str(e)
        return HttpResponseBadRequest(json.dumps(ee))

    def create_inactive_user(self, form):
        p = id_generator()
        new_user                   = form.save(commit=False)
        new_user.is_active         = True
        new_user.username          = form.cleaned_data["dni"]
        new_user.password_default  = p
        new_user.set_password(p)
        new_user.save()

        for i in self.request.POST.getlist('profiles'):
            new_user.profiles.add(int(i))

        return new_user

class update_users(LoginRequiredMixin, UpdateView):
    login_url= reverse_lazy('error404')
    template_name = "form_user.html"
    model = Users
    form_class  = UsuarioCreationForm
    exclude = ('password1','password1','email',)
    success_url = reverse_lazy('user-completado')

    def get_context_data(self, **kwargs):
        context = super(update_users, self).get_context_data(**kwargs)
        context['name'] = 'Actualizar'
        return context



    def form_invalid(self, form):
        ee = {}
        if form.errors:
            for i in form.errors:
                e = form.errors[i]
                ee[i] = str(e)
        return HttpResponseBadRequest(json.dumps(ee))



def fotoperfil(r):
    a     = get_object_or_404(Users, pk=r.user.id)
    formu = formFoto(r.POST,r.FILES, instance=a)
    print(formu)
    if formu.is_valid():
        formu.save()
    return HttpResponse("foto guardada correctamente")

def usuario(r):
    return render(r, "usuario.html")

def guardaperfil(r):
    a=get_object_or_404(Users, pk=r.user.id)
    form=cueform(r.POST,instance=a)
    if form.is_valid():
        form.save()
        return HttpResponse("guardado")
    else:
        if r.is_ajax():
            ee = {}
            if form.errors:
                for i in form.errors:
                    e = form.errors[i]
                    ee[i] = str(e)

            return HttpResponseBadRequest(json.dumps(ee))

def usuariosCompletado(r):
    return HttpResponse("ok registrado")