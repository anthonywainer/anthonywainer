from django.shortcuts import render, get_object_or_404, HttpResponse
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required

from apps.security.forms import UserChangeForm, formFoto
from apps.security.models import Users, modules, detail_permission_profile_modules

class paginaweb(TemplateView):
    template_name = "login.html"

# Create your views here.
@login_required(login_url='login/')
def index(r):

    u = Users.objects.filter(id= r.user.id).values('profiles','profiles__description')
    template_name = 'index.html'
    if len(u) > 1:
        idprofile = r.GET.get("sys")

        if idprofile is None:
            p = {'profile':u}
            template_name = 'system.html'
            return render(r,template_name,p)

    else:
        idprofile = u[0]['profiles']

    queryset = detail_permission_profile_modules.objects.select_related('idmodules').filter(idmodules__status = True, idprofile_id=idprofile).values(
                    'idmodules_id','idmodules__description','idmodules__url','idmodules__father',
                    'idmodules__icon__clase',
                    'See', 'idprofile__description'
                    )
    profile_des = queryset[0]['idprofile__description']
    p = {'modulo':queryset, 'profile':u,'profile_des':profile_des}
    return render(r,template_name,p)



def lmodulos(m):
    return render(m, 'modulos.html')


def fotoperfil(r):
    a     = get_object_or_404(Users,pk=r.user.id)
    formu = formFoto(r.POST, r.FILES, instance=a)
    if formu.is_valid():
        formu.save()
    return HttpResponse("ok")

def usuario(r):
    return render(r, "usuario.html")


def guardarpadre(r):
    f=UserChangeForm(r.POST)
    f.save()
    return HttpResponse("guardado correctamente")
