from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin, Group
)

from django.core import validators
from django.db import models
from django.utils import timezone
from django.utils.safestring import mark_safe

from apps.core.core import TimeStampedModel
class icons(models.Model):
    description = models.CharField(max_length=100)
    clase = models.CharField(max_length=100)
    
    def __str__(self):
        return mark_safe("<li class='" + self.clase + "'><li> " + self.description)

class modules(TimeStampedModel):
    description = models.CharField(max_length=50)
    father = models.IntegerField(default=0)
    url = models.CharField(max_length=150)
    icon = models.ForeignKey(icons, null=True, related_name='idicon')
    
    def __str__(self):
        return self.description
class profile(TimeStampedModel):
    description = models.CharField(max_length=50)
    module      = models.ManyToManyField(modules, through="detail_permission_profile_modules")
    
    def __str__(self):
        return self.description

class detail_permission_profile_modules(TimeStampedModel):
    idprofile = models.ForeignKey(profile)
    idmodules = models.ForeignKey(modules)
    See = models.BooleanField(default=True)
    Search = models.BooleanField(default=True)
    Add = models.BooleanField(default=True)
    Update = models.BooleanField(default=True)
    Delete = models.BooleanField(default=True)
    Print = models.BooleanField(default=True)
    Export = models.BooleanField(default=True)
    Import = models.BooleanField(default=True)
    


class MyUserManager(BaseUserManager):
    def create_user(self, email, username, dni, password=None):
        if not email:
            raise ValueError('El usuario tiene una dirección email incorrecta')

        if not dni:
            raise ValueError('El usuario tiene dni incorrecta')

        user = self.model(
            email    = self.normalize_email(email),
            username = username,
            dni      = dni,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, dni, password):
        
        user = self.create_user(email,
            password = password,
            username = username,
            dni      = dni
        )
        user.is_admin = True
        user.save(using=self._db)
        return user



class Users(AbstractBaseUser, PermissionsMixin, TimeStampedModel ):
    username = models.CharField(
        ('Usuario'),
        max_length  = 30,
        unique      = True,
        help_text   = ('Requiere: 30 carácteres o menos. Letras, digitos y  @/./+/-/_ sólo.'),
        validators  = [
            validators.RegexValidator(
                r'^[\w.@+-]+$',
               ('Ingrese un usuario válido. Este valor solo podrá contener '
                 'Letras, Números ' 'and @/./+/-/_ Carácteres.')
            ),
        ],
        error_messages={
            'unique': ("Ya existe usuario con ese nombre"),
        },
    )
    names            = models.CharField(max_length=50,null=True, verbose_name='Nombres')
    first_name       = models.CharField(max_length=50,null=True, verbose_name='Apellido Paterno')
    last_name        = models.CharField(max_length=50,null=True, verbose_name='Apellido Materno')
    dni              = models.CharField(
        max_length=8,
        unique=True,
        help_text=('DNI Requiere: 8 carácteres obligatorios'),
        validators=[
            validators.RegexValidator(
                r'^[\w.@+-]+$',
                ('Ingrese un dni válido. Este valor solo podrá contener '
                 'Números' 'de 8 Dígitos.')
            ),
        ],
        error_messages={
            'unique': ("Ya existe dni, probar con otro"),
        },
    )
    Sex = (
        ('f', 'Femenino'),
        ('m', 'Masculino'),
    )

    email            = models.EmailField(verbose_name='Dirección de email',max_length=255, unique=True, blank=True, null=True)
    date_birth       = models.DateField(null=True, verbose_name='Fecha de Cumpleaños')
    foto             = models.ImageField(max_length=500, upload_to='fotoperfil', null=True)
    cellphone        = models.CharField(max_length=100, null=True, verbose_name='Celular')
    telephone        = models.CharField(max_length=100, null=True)
    address          = models.CharField(max_length=200,null=True)
    work_experiencie = models.CharField(max_length=30, null=True)
    sex              = models.CharField(max_length=1, choices=Sex)
    password_default = models.CharField(max_length=15, null=True)

    profiles = models.ManyToManyField(profile, blank=True)
    
    is_staff = models.BooleanField(
        ('staff status'),
        default=False,
        help_text=('Indica si el usuario puede iniciar sesión en este sitio de administración.'),
    )
    is_active = models.BooleanField(
        ('active'),
        default=True,
        help_text=(
            'Designa si el usuario debe ser tratado como activo. '
            'Seleccionarla en lugar de eliminar las cuentas .'
        ),
    )
    date_joined = models.DateTimeField(('fecha de Inscripción'), default=timezone.now)

    is_admin         = models.BooleanField(default=False)
    objects          = MyUserManager()

    USERNAME_FIELD   = 'username'
    REQUIRED_FIELDS  = ['dni','email']

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.username

    def __str__(self):              # __unicode__ on Python 2
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

