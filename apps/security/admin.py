from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from simple_history.admin import SimpleHistoryAdmin

from .models import Users,  profile, modules
from .forms import UserChangeForm, UserCreationForm

class ProfiileInlineAdmin(admin.TabularInline):
    model = Users.profiles.through


class UserAdmin(BaseUserAdmin, SimpleHistoryAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('username','email', 'dni', 'is_admin')
    list_filter = ('is_admin',)
    fieldsets = (
        (None, {'fields': ('username','email', 'password')}),
        ('Foto Perfil', {'fields': ('foto',)}),
        ('Información Personal', {'fields': ('names','first_name','last_name','dni')}),
        #('Redes Sociales', {'fields': ('redsocial',)}),
        ('Permisos', {'fields': ('is_admin','is_active')}),
    )
    inlines = (ProfiileInlineAdmin,)
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username','email', 'dni', 'password1', 'password2')}
        ),
    )
    search_fields = ('email','dni','username')
    ordering = ('email','dni','username')
    filter_horizontal = ()


#class permisosInlineAdmin(admin.TabularInline):
    #model = perfil.modulo.through
    
#class perfilAdmin(admin.ModelAdmin):
    #inlines = (permisosInlineAdmin,)

# Now register the new UserAdmin...
admin.site.register(Users, UserAdmin)
#admin.site.register(perfil, perfilAdmin )
#admin.site.register(social_network)
admin.site.register(profile, SimpleHistoryAdmin)
admin.site.register(modules, SimpleHistoryAdmin)
# ... and, since we're not using Django's built-in permissions,
# unregister the Group model from admin.
admin.site.unregister(Group)

