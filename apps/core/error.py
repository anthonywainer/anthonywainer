from django.http import HttpResponseBadRequest
from django.views.generic import TemplateView
import json

def error(f):
    ee = {}
    for i in f.errors:
        e = f.errors[i]
        ee[i] = str(e)

    return HttpResponseBadRequest(json.dumps(ee))
                # recoger los errores y enviarlo al servidor

class error404(TemplateView):
    template_name = 'error404.html'
