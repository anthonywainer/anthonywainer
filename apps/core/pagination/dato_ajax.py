#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render
from apps.security.models import Users
from django.db.models import Max, Q

def datos_ajax(r):
    dat = r.GET.get('campo')
    tabla= r.GET.get('tabla')
    camp = eval(r.GET.get('list'))
    v = r.GET.get('values')
    uu = eval(tabla)
    st = ''
    c = 0
    for i in camp:
        cad = "Q(" + i + "__contains = dat)"
        if c == 0:
            st += cad
        else:
            st += "|"+cad
        c+=1
    s= eval(st)

    pad = eval("uu.objects.filter(s).values("+v+")[:10]")
    modulo = {'lista':pad,'nroP':0}
    return render(r, 'datoAjax'+tabla+'.html', modulo)