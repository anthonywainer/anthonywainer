from django.contrib.auth.decorators import login_required
from functools import partial

class LoginRequiredMixin(object):
    login_url = None

    def _login_required(self):
        return partial(login_required, login_url=self.login_url)

    def dispatch(self, request, *args, **kwargs):
        def inner(*args, **kwargs):
            klass = LoginRequiredMixin
            return super(klass, self).dispatch(*args, **kwargs)

        return partial(inner, login_url=self.login_url)(request, *args, **kwargs)