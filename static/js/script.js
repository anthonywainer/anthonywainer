$(window).hashchange( function() {
    // Alerts every time the hash changes!
    str = location.hash;
    if (str.length >= 1) {
        str = str.substring(1, str.length);
        traemedatos(str);
    }else{
        location.reload();
    }
  })

str = window.location.hash;
if (str.length >= 1){
    str = str.substring(1, str.length);
    traemedatos(str);
}


$(window).load(function(){
    setTimeout(function(){
        $('#loading').fadeOut(400,"linear");
    },300);
});

function traemedatos(u){
     $.ajax({
         type: 'GET',
         url: u,
         //data: {'nombre':n},
         success: function (d) {
             $('#resultados').empty().html(d);
         },
         error: function (f) {
             $('#resultados').empty().html(f.responseText);
         },
         beforeSend: function(xhr, status) {
            // TODO: show spinner
            $('#spinner').fadeIn(400,"linear");
        },
        complete: function() {
            // TODO: hide spinner
            $('#spinner').fadeOut(400,"linear");
        }
     });
}
    function noti(type,msg){
        Lobibox.notify(type, {
        size: 'mini',
        rounded: true,
        delayIndicator: true,
        msg: msg
    });
    }
