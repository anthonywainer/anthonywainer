$("form").submit(function () {
    w=$(this).serialize();
    $.post("",w,function(data){
        unblock_form();
        location.reload();
    }).fail(function(resp) {
        unblock_form();
        var errors = JSON.parse(resp.responseText);
        console.log(resp);
        for (e in errors) {
            if (e == '__all__'){
                $('#msmlogin').html(errors[e]);
            }
            var id = '#id_' + e;
            $(id).after(errors[e]);
        }
    });
    return false; 
});

function unblock_form() {
        $('.errorlist').remove();
    }
