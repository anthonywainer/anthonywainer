function unblock_form() {
    $('.errorlist').remove();
    $('.popover').remove();
    $("input").css({border:'1px solid #aaa'});
}function unblock_form2() {
    $('.errorlist').remove();
}

function errorf(r){
    unblock_form(r);
    var errors = JSON.parse(r.responseText);
    for (e in errors) {     
                var id = '[for="id_' + e + '"]';
                $("#id_"+e).css({border:'1px solid red'});
                $(id).attr('data-content',errors[e]);
                $(id).popover({animation:true, html:true, placement: "top"});
                $(id).popover('show');
                
    }
    $('.popover').css({border:'1px dashed red', top:'40px' });
    $('.popover-content').css('padding','0px');
}

function errornormal(r,form){
    unblock_form2();
    var errors = JSON.parse(r.responseText);
            for (e in errors) {
                var id = '#'+form+' #id_' + e;
                $(id).after(errors[e]);
            }
}

function Modal(id,title,body,footer,taman){
  var mod = ''+
  '<div class="modal fade" id="'+id+'" role="dialog" aria-hidden="true">' +
    '<div class="modal-dialog '+taman+'">' +
      '<div class="modal-content">' +
        '<div class="modal-header">' +
          '<button type="button" class="close" onclick="cerrarModal(\''+id+'\')" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
          '<h4 class="modal-title" align="center">'+title+'</h4>' +
        '</div>' +
        '<div class="modal-body">' + body +'</div>'+
        '<div class="modal-footer">'+footer+'</div>' +
      '</div>' +
    '</div>' +
  '</div>';
  return mod;
}

function cerrarModal(id){
  $('#'+id).modal('hide');
}
