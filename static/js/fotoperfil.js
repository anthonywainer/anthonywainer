//Lee el tipo MIME de la cabecera de la imagen
datoscuenta();
function datoscuenta(){
    $.get("security/datoscuenta",function(data){
        $("#impri").html(data);
    });
}

function unblock_form() {
    $('.errorlist').remove();
}

function guardar_perfil(){
    form=$("#form_gurcuenta").serialize();
    $.post("security/guardaprofile",form,function(data){
        $("#editar").empty();
        datoscuenta();
    }).fail(function(resp) {
            unblock_form();
           var errors = JSON.parse(resp.responseText);
            for (e in errors) {    
                var id = '#id_' + e;
                $(id).after(errors[e]);
            }

          }); ;
}

function obtenerTipoMIME(cabecera) {
    return cabecera.replace(/data:([^;]+).*/, '\$1');
}

function edi(){
    $.get('security/cuedi',function(data){
        $("#editar").html(data);
    });
}

function contra(){
        $('#editar').empty().html('<iframe  width="300px" height="700px" frameborder="0" scrolling="" src="password"></iframe>');
}

function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            origen = e.target; //objeto FileReader
            tipo = obtenerTipoMIME(origen.result.substring(0, 30));
            if (tipo !== 'image/jpeg' && tipo !== 'image/png' && tipo !== 'image/gif') {
                AlDelete('El formato de imagen no es válido: debe seleccionar una imagen JPG, PNG o GIF.');
            } else {
                $('#img').attr('src', origen.result);
                $('.di').css('display','inline');
                $('.perfilI').css('border','5px solid white');
              
            }
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function(){
    readURL(this);
});
$(".re").click(function(){
  $('.re').css("display","none");
  $(".imgInp").css("display",'inline'   );
  $(".xx").css("display",'inline'   );
  $('.perfilI').css('border','5px dashed lightblue');
});
$('.re').mouseenter(function(){
    $(".pen").css("display",'inline');
}).mouseleave(function(){
    $(".pen").css("display",'none');
});

function cancel(){
    $('#img').attr('src', " ");   
    $('.di').css('display','none'); 
    $('.re').css('display','inline'); 
    $('.perfilI').css('border','none');
}
function guardarI(url){
    formData = new FormData($('#formularioImg')[0]);

    console.log(formData);
        $.ajax({
            url: url,  
            type: 'POST',
            data: formData,
            //necesario para subir archivos via ajax
            cache: false,
            contentType: false,
            processData: false,

            success: function(data){
                $("#foto_perfil").attr("src","/media/"+data);
                $("#foto_perfil1").attr("src","/media/"+data);
                a=$('#img').attr('src');
                $('#img').attr('src'," ");
                $("#mos_img").attr('src',a);
                $('.di').css('display','none'); 
                $('.re').css('display','inline');
                $('.perfilI').css('border','none');
            }

        });
}
// getElementById
    function $id(id) {
        return document.getElementById(id);
    }


    // output information
    function Output(msg) {
        var m = $id("messages");
        m.innerHTML = msg + m.innerHTML;
    }


    // file drag hover
    function FileDragHover(e) {
        e.stopPropagation();
        e.preventDefault();
        e.target.className = (e.type == "dragover" ? "hover" : "");
    }


    // file selection
    function FileSelectHandler(e) {

        // cancel event and hover styling
        FileDragHover(e);

        // fetch FileList object
        var files = e.target.files || e.dataTransfer.files;

        // process all File objects
        for (var i = 0, f; f = files[i]; i++) {
            ParseFile(f);
        }

    }


    // output file information
    function ParseFile(file) {

        Output(
            "<p>File information: <strong>" + file.name +
            "</strong> type: <strong>" + file.type +
            "</strong> size: <strong>" + file.size +
            "</strong> bytes</p>"
        );

    }


    // initialize
    function Init() {

        var fileselect = $id("imgInp"),
            filedrag = $id("perfilI"),
            submitbutton = $id("submitbutton");
    }

    // call initialization file
    if (window.File && window.FileList && window.FileReader) {
        Init();
    }