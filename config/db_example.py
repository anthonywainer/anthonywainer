DATABASES = {
    'default': {
       'ENGINE'  : 'django.db.backends.postgresql_psycopg2',
       'NAME'    : 'emmarc',
       'USER'    : 'emmarc',
       'PASSWORD': 'emmarc',
       'HOST'    : 'localhost',
       'PORT'    : '5432',
    }
}