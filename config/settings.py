from .db import DATABASES
from .base import *

DEBUG = True

ALLOWED_HOSTS = ['www.testpsicologico.com']

DOMAIN_URL = "testpsicologico.com"